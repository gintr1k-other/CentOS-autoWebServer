#!/bin/bash

## Переменные
Apache=true
PHP=true
MariaDB=true
CertBot=false
phpMyAdmin=false
Settings=true

Settings_timezone='Europe/Moscow'
Settings_packages='htop nano iptables-services'

function dialog_message() {
	# $1 - Вопрос
	# $2 - Ответ по-умолчанию
	while true; do
		read -e -p "$1 [Y/n]: " -i "$2" yn
		case $yn in
			[Yy]* ) return 0; break;;
			[Nn]* ) return 1;;
			* ) echo 'Ответьте Y(es) или N(o)'
				echo ' ';;
		esac
	done
}

function package_check() {
	clear
	echo -e 'Автор данного скрипта \e[1;31mGinTR1k\e[0m - \e[1;33;4;44mhttps://g1k.me/\e[0m'
	echo ' '
	echo 'Ожидается установка и настройка:'
	if $Apache
	then
		echo '	- Apache (httpd)'
	fi
	
	if $PHP
	then
		echo '	- PHP 7.1'
	fi
	
	if $MariaDB
	then
		echo '	- MariaDB'
	fi
	
	if $CertBot
	then
		echo '	- CertBot (LetsEncrypt)'
	fi
	
	if $phpMyAdmin
	then
		echo '	- phpMyAdmin'
	fi
	
	if $Settings
	then
		echo '	- настройка сервера'
	fi
	
	echo ' '
}

function message() {
	package_check
	
	## Apache
	if  dialog_message 'Установить Apache' '';
	then
		Apache=true
	else
		Apache=false
	fi
	package_check
	
	## PHP
	if  dialog_message 'Установить PHP' '';
	then
		PHP=true
	else
		PHP=false
	fi
	package_check
	
	## MariaDB
	if  dialog_message 'Установить MariaDB' '';
	then
		MariaDB=true
	else
		MariaDB=false
	fi
	package_check
	
	## CertBot
	if  dialog_message 'Установить CertBot (LetsEncrypt)' '';
	then
		CertBot=true
	else
		CertBot=false
	fi
	package_check
	
	## phpMyAdmin
	if  dialog_message 'Установить phpMyAdmin' '';
	then
		phpMyAdmin=true
	else
		phpMyAdmin=false
	fi
	package_check
	
	## Settings
	if  dialog_message 'Настроить сервер (время, изменение порта SSH и т.д.)' '';
	then
		Settings=true
	else
		Settings=false
	fi
	package_check
	
	## Последний вопрос
	if  dialog_message 'Продолжить установку' '';
	then
		installation
	else
		echo 'Установка отменена'
	fi
}

function installation() {
	clear
	
	## Обновляем систему и подключаем репозитории
	echo 'Обновление системы и установка дополнительных пакетов'
	echo ' '
	sleep 3
	
	yum update -y
	yum install epel-release
	rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
	
	if $Settings
	then
		echo ' '
		echo ' '
		echo 'Будут установлены следующие пакеты: '
		echo $Settings_packages
		sleep 10
		yum install -y $Settings_packages
		
		systemctl stop firewalld
		systemctl disable firewalld
		systemctl enable iptables
		systemctl start iptables
		
		if dialog_message 'Иземнить порт SSH' 'y';
		then
			read -p "Выберите порт (От 1024 до 65535): " sshportconfig
			if (( ("$sshportconfig" > 1024) && ("$sshportconfig" < 65535) ));
			then
				echo "Port $sshportconfig" >> /etc/ssh/sshd_config
				iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport $sshportconfig -j ACCEPT
				iptables -A INPUT -p tcp --destination-port 22 -j DROP
				iptables -I INPUT -p tcp --dport 80 -j ACCEPT
				iptables -I INPUT -p tcp --dport 443 -j ACCEPT
				service iptables save
				systemctl restart sshd
			else
				echo 'Введеное число выходит за доступные диапозоны'
				echo 'Установка порта отменена'
				sleep 4
			fi
		fi
		
		mv /etc/localtime /etc/localtime.bak
		ln -s /usr/share/zoneinfo/$Settings_timezone /etc/localtime
	fi

	## Настраиваем Apache
	if $Apache
	then
		echo 'Установка и настройка Apache'
		echo ' '
		sleep 3
		
		yum install -y httpd mod_ssl
		systemctl enable httpd
		systemctl start httpd
		mkdir /web
		echo '/web/*/logs/*.log' | cat - /etc/logrotate.d/httpd > temp && mv temp /etc/logrotate.d/httpd
		
		## Настройка SSL
		sed -i -e "/SSLProtocol all -SSLv2/s/^#*/#/" /etc/httpd/conf.d/ssl.conf
		sed -i -e "/SSLCipherSuite HIGH:MEDIUM:!aNULL:!MD5:!SEED:!IDEA/s/^#*/#/" /etc/httpd/conf.d/ssl.conf
		echo 'SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH' >> /etc/httpd/conf.d/ssl.conf
		echo 'SSLProtocol All -SSLv2 -SSLv3' >> /etc/httpd/conf.d/ssl.conf
		echo 'SSLHonorCipherOrder On' >> /etc/httpd/conf.d/ssl.conf
		echo '# Disable preloading HSTS for now.  You can use the commented out header line that includes' >> /etc/httpd/conf.d/ssl.conf
		echo '# the "preload" directive if you understand the implications.' >> /etc/httpd/conf.d/ssl.conf
		echo '#Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains; preload"' >> /etc/httpd/conf.d/ssl.conf
		echo 'Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains"' >> /etc/httpd/conf.d/ssl.conf
		echo 'Header always set X-Frame-Options DENY' >> /etc/httpd/conf.d/ssl.conf
		echo 'Header always set X-Content-Type-Options nosniff' >> /etc/httpd/conf.d/ssl.conf
		echo '# Requires Apache >= 2.4' >> /etc/httpd/conf.d/ssl.conf
		echo 'SSLCompression off' >> /etc/httpd/conf.d/ssl.conf
		echo 'SSLUseStapling on' >> /etc/httpd/conf.d/ssl.conf
		echo 'SSLStaplingCache "shmcb:logs/stapling-cache(150000)"' >> /etc/httpd/conf.d/ssl.conf
		echo '# Requires Apache >= 2.4.11' >> /etc/httpd/conf.d/ssl.conf
		echo '# SSLSessionTickets Off' >> /etc/httpd/conf.d/ssl.conf
	fi

	## Устанавливаем PHP
	if $PHP
	then
		echo 'Установка и настройка PHP'
		echo ' '
		sleep 3
		
		yum install -y mod_php71w php71w-cli php71w-common php71w-gd php71w-mbstring php71w-mcrypt php71w-mysqlnd php71w-xml
		
		## Настройка PHP
		echo "php_admin_value date.timezone 'Europe/Moscow'" >> /etc/httpd/conf.d/php.conf
		echo 'php_admin_value max_execution_time 60' >> /etc/httpd/conf.d/php.conf
		echo 'php_admin_value upload_max_filesize 30M' >> /etc/httpd/conf.d/php.conf
	fi

	## MariaDB
	if $MariaDB
	then
		echo 'Установка и настройка MariaDB'
		echo ' '
		sleep 3
		
		yum install -y mariadb mariadb-server
		systemctl enable mariadb
		systemctl start mariadb
		clear
		echo '*** ПРОИЗВОДИТСЯ УСТАНОВКА MARIADB ***'
		echo '*** ПРОИЗВОДИТСЯ УСТАНОВКА MARIADB ***'
		echo '*** ПРОИЗВОДИТСЯ УСТАНОВКА MARIADB ***'
		echo ' '
		echo 'От вас требуется все время тыкать Enter, а при вводе пароля'
		echo -e 'ввести \e[1;31mновый пароль\e[0m для пользотеля \e[1mroot\e[0m в MariaDB,'
		echo 'отличный от пароля системного пользователя'
		sh /usr/bin/mysql_secure_installation
		systemctl restart mariadb
	fi

	## CertBot
	if $CertBot
	then
		echo 'Установка CertBot (LetsEncrypt)'
		echo ' '
		sleep 3
		
		yum install -y python-certbot-apache
		certbot --apache
		
		## Обновление сертификатов в crontab
		#write out current crontab
		crontab -l > mycron
		#echo new cron into cron file
		echo "30 2 * * * /usr/bin/certbot renew >> /var/log/le-renew.log" >> mycron
		#install new cron file
		crontab mycron
		rm mycron
	fi
	
	echo '+---------------------+'
	echo -e "| \e[1;32mУстановка завершена\e[0m |"
	echo '+---------------------+'
}

message
echo ' '
echo '+-----------------------------+'
echo -e "| \e[1;32mСкрипт завершил свою работу\e[0m |"
echo '+-----------------------------+'
